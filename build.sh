#!/bin/bash

# Check if there is already dockify:dev image available
AVAILABLE_IMAGE=$(docker image ls --format "{{.Repository}}:{{.Tag}}" | grep dockify:dev)

# If set to true, build even if the image already exists
FORCE_REBUILD=$1

if [ ! -z $FORCE_REBUILD ] || [ -z $AVAILABLE_IMAGE ]; then
    echo 'Building dockify:dev image'
    docker build -t dockify:dev .
fi