FROM registry.gitlab.com/dc-images/php/7.2-cli-alpine/dev:v2.0.0

# Install docker client
ENV DOCKERVERSION=18.09.3
RUN curl -fsSLO https://download.docker.com/linux/static/stable/x86_64/docker-${DOCKERVERSION}.tgz \
  && tar xzvf docker-${DOCKERVERSION}.tgz --strip 1 \
                 -C /usr/local/bin docker/docker \
  && rm docker-${DOCKERVERSION}.tgz

# Add composer bin directory to path
ENV PATH /data/vendor/bin:$PATH

RUN \
  mkdir -m 0775 -p /storage/composer

COPY . /data
WORKDIR /data

ENTRYPOINT [ "/data/application.php" ]
