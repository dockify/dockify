#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

if (!function_exists('exitWithError')) {
    function exitWithError(int $code, string $message)
    {
        echo sprintf(
            'An error occurred during app execution: [ErrNo %d] %s',
            $code,
            $message
        );

        exit(1);
    }
}

$containerBuilder = new ContainerBuilder();

$loader = new YamlFileLoader($containerBuilder, new FileLocator(__DIR__));

try {
    $loader->load('config/services.yml');
} catch (\Exception $exception) {
    exitWithError(
        $exception->getCode(),
        $exception->getMessage()
    );
}

$containerBuilder->compile();

try {
    $app = $containerBuilder->get(\Dockify\App::class);
    $app->run();
} catch (\Exception $exception) {
    exitWithError(
        $exception->getCode(),
        $exception->getMessage()
    );
}
