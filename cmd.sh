#!/bin/bash

# Constants
DEV_IMAGE_NAME="dockify:dev"

# ==========================
# ===  HELPER FUNCTIONS  ===
# ==========================

build_image() {
    docker build \
        -t ${DEV_IMAGE_NAME} \
        -f docker/dev/Dockerfile \
        .
}

build_if_not_exists() {
    EXISTING_IMAGES=$(docker image ls --format "{{.Repository}}:{{.Tag}}" | grep ${DEV_IMAGE_NAME})

    if [[ -z ${EXISTING_IMAGES} ]]; then
        echo "Dev image for $DEV_IMAGE_NAME not found, building it first..."
        build_image
    fi
}

exec_command() {
    build_if_not_exists

    USER_ID=$UID

    if [[ "$1" == "run" ]]; then
        USER_ID=0
        shift
    fi

    docker run \
        -it \
        -v $PWD:/data \
        -v $PWD/storage/cache:/cache \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -e COMPOSER_HOME=/cache/composer \
        --rm \
        --user ${USER_ID}:${USER_ID} \
        --entrypoint /bin/sh \
        --name dockify \
    "$DEV_IMAGE_NAME" -c "$@"
}

# ==================
# ===  COMMANDS  ===
# ==================

case "$1" in
    "exec")
        exec_command "${@:2}"
    ;;

    "run")
        exec_command run "/data/application.php run --pwd=$PWD/storage/tmp --uid=$UID"
    ;;

    "update")
        exec_command run "/data/application.php update"
    ;;

    "build")
        build_image
    ;;
esac
