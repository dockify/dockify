<?php

namespace Dockify\Docker\Command;

use Dockify\Docker\Contract\DockerCommandInterface;

class DockerRun implements DockerCommandInterface
{
    /**
     * @var string
     */
    private $image = 'hello-world';

    /**
     * @var array
     */
    private $command = ['docker', 'run'];

    /**
     * @var array
     */
    private $cmd = [];

    /**
     * @param string $image
     * @return DockerRun
     */
    public function image(string $image): DockerRun
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @param string $entrypoint
     * @return $this
     */
    public function entrypoint(string $entrypoint): DockerRun
    {
        $this->command[] = '--entrypoint';
        $this->command[] = $entrypoint;

        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function env(string $key, string $value): DockerRun
    {
        $this->command[] = '-e';
        $this->command[] = sprintf('%s=%s', $key, $value);

        return $this;
    }

    /**
     * @return DockerRun
     */
    public function tty(): DockerRun
    {
        $this->command[] = '-it';

        return $this;
    }

    /**
     * @return DockerRun
     */
    public function remove(): DockerRun
    {
        $this->command[] = '--rm';

        return $this;
    }

    /**
     * @param string $user
     * @param string $group
     * @return DockerRun
     */
    public function user(string $user, string $group = ''): DockerRun
    {
        if ($group === '') {
            $group = $user;
        }

        $this->command[] = '--user';
        $this->command[] = "$user:$group";

        return $this;
    }

    /**
     * @param string $src
     * @param string $dest
     * @return DockerRun
     */
    public function volume(string $src, string $dest): DockerRun
    {
        $this->command[] = '-v';
        $this->command[] = "$src:$dest";

        return $this;
    }

    /**
     * @param array $cmd
     * @return DockerRun
     */
    public function cmd(array $cmd): DockerRun
    {
        $this->cmd = $cmd;

        return $this;
    }

    /**
     * @return array
     */
    public function get(): array
    {
        return array_merge($this->command, [$this->image], $this->cmd);
    }
}
