<?php

namespace Dockify\Docker\Command;

use Dockify\Docker\Contract\DockerCommandInterface;

class DockerVolume implements DockerCommandInterface
{
    /**
     * @var array
     */
    private $command = ['docker', 'volume'];

    /**
     * @var array
     */
    private $subCommand = [];

    /**
     * @param string $volumeName
     * @return DockerVolume
     */
    public function create(string $volumeName): DockerVolume
    {
        $this->subCommand = ['create', $volumeName];

        return $this;
    }

    /**
     * @param string $volumeName
     * @return DockerVolume
     */
    public function remove(string $volumeName): DockerVolume
    {
        $this->subCommand = ['rm', $volumeName];

        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function ls(array $params)
    {
        $this->subCommand = array_merge(['ls'], $params);

        return $this;
    }

    public function get(): array
    {
        return array_merge($this->command, $this->subCommand);
    }
}
