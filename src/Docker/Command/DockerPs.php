<?php

namespace Dockify\Docker\Command;

use Dockify\Docker\Contract\DockerCommandInterface;

class DockerPs implements DockerCommandInterface
{
    /**
     * @var array
     */
    private $command = ['docker', 'ps'];

    /**
     * @var string
     */
    private $format = '';

    /**
     * @param string $format
     * @return DockerPs
     */
    public function format(string $format): DockerPs
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @param string $filter
     * @param string $value
     * @return DockerPs
     */
    public function filter(string $filter, string $value): DockerPs
    {
        $this->command[] = '--filter';
        $this->command[] = sprintf('%s=%s', $filter, $value);

        return $this;
    }

    public function get(): array
    {
        if ($this->format !== '') {
            $this->command[] = '--format';
            $this->command[] = sprintf('"%s"', $this->format);
        }

        return $this->command;
    }
}
