<?php

namespace Dockify\Docker\Command;

use Dockify\Docker\Contract\DockerCommandInterface;

class DockerPull implements DockerCommandInterface
{
    /**
     * @var array
     */
    private $command = ['docker', 'pull'];

    /**
     * @var string
     */
    private $image = '';

    /**
     * @param string $image
     * @return DockerPull
     */
    public function image(string $image): DockerPull
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return array
     */
    public function get(): array
    {
        $this->command[] = $this->image;

        return $this->command;
    }
}
