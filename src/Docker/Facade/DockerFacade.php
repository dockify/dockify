<?php

declare(strict_types=1);

namespace Dockify\Docker\Facade;

use Dockify\Creator;
use Dockify\Docker\Docker;
use Dockify\Docker\Exception\VolumeDoesNotExistException;

use Symfony\Component\Process\Process;

class DockerFacade
{
    /**
     * @var Docker
     */
    public $docker;

    public function __construct(Docker $docker)
    {
        $this->docker = $docker;
    }

    /**
     * @param string $image
     * @return Process
     */
    public function pull(string $image): Process
    {
        $process = new Process(
            $this->docker->pull()
                ->image($image)
                ->get()
        );
        $process->setTty(true);
        $process->run();

        return $process;
    }

    /**
     * @param string $containerID
     * @return string
     */
    public function getContainerImage(string $containerID): string
    {
        ($process = new Process(
            $this->docker->ps()
                ->filter('id', $containerID)
                ->format('{{.Image}}')
                ->get()
        ))->run();

        return trim($process->getOutput(), "\"\n");
    }

    /**
     * @param string $uid
     * @param string $pwd
     * @param Creator $creator
     * @param string $volumeName
     * @return Process
     */
    public function runDockify(
        string $uid,
        string $pwd,
        Creator $creator,
        string $volumeName
    ): Process {
        $process = new Process(
            $this->docker->run()
                ->image($creator->getDockerImage())
                ->tty()
                ->remove()
                ->user($uid)
                ->volume($pwd, $creator->getMountPoint())
                ->volume($volumeName, $creator->getCachePath())
                ->cmd([$creator->getCommand()])
                ->get()
        );
        $process->setTty(true);
        $process->run();

        return $process;
    }

    /**
     * @param string $volumeName
     * @return Process
     */
    public function createVolume(string $volumeName): Process
    {
        $process = new Process(
            $this->docker->volume()
                ->create($volumeName)
                ->get()
        );
        $process->setTty(true);
        $process->run();

        return $process;
    }

    /**
     * @param string $volumeName
     * @return bool
     */
    public function volumeExists(string $volumeName): bool
    {
        ($process = new Process(
            $this->docker->volume()
                ->ls(['-q', '--format', '{{.Name}}'])
                ->get()
        ))->run();

        return strpos($process->getOutput(), $volumeName) !== false;
    }

    /**
     * @param string $volumeName
     * @return void
     */
    public function createVolumeIfNotExists(string $volumeName): void
    {
        if (!$this->volumeExists($volumeName)) {
             $this->createVolume($volumeName);
        }
    }

    /**
     * @param string $volumeName
     * @param string $path
     * @param string $uid
     * @return Process
     * @throws VolumeDoesNotExistException
     */
    public function changeVolumeDirectoryOwnership(
        string $volumeName,
        string $path,
        string $uid
    ): Process {
        if (!$this->volumeExists($volumeName)) {
            throw new VolumeDoesNotExistException(sprintf(
                'Unknown volume "%s"',
                $volumeName
            ));
        }

        $path = sprintf('/%s', ltrim($path));

        $process = new Process(
            $this->docker->run()
                ->image('alpine')
                ->tty()
                ->remove()
                ->volume($volumeName, $path)
                ->cmd([
                    'chown',
                    '-R',
                    sprintf('%s:%s', $uid, $uid),
                    $path
                ])
                ->get()
        );
        $process->setTty(true);
        $process->run();

        return $process;
    }
}
