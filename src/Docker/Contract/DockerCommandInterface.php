<?php

namespace Dockify\Docker\Contract;

interface DockerCommandInterface
{
    /**
     * @return array
     */
    public function get(): array;
}
