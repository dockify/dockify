<?php

namespace Dockify\Docker;

use Dockify\Docker\Command\DockerPs;
use Dockify\Docker\Command\DockerPull;
use Dockify\Docker\Command\DockerRun;
use Dockify\Docker\Command\DockerVolume;

class Docker
{
    /**
     * @return DockerPull
     */
    public function pull(): DockerPull
    {
        return new DockerPull();
    }

    /**
     * @return DockerPs
     */
    public function ps(): DockerPs
    {
        return new DockerPs();
    }


    /**
     * @return DockerRun
     */
    public function run(): DockerRun
    {
        return new DockerRun();
    }

    /**
     * @return DockerVolume
     */
    public function volume(): DockerVolume
    {
        return new DockerVolume();
    }
}
