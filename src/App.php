<?php

declare(strict_types=1);

namespace Dockify;

use Dockify\Command\RunCommand;
use Dockify\Command\UpdateCommand;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Application;

class App
{
    /**
     * @var Application
     */
    private $console;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, Application $application)
    {
        $this->container = $container;
        $this->console = $application;
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        $this->console->addCommands([
            $this->container->get(RunCommand::class),
            $this->container->get(UpdateCommand::class),
        ]);

        $this->console->run();
    }
}
