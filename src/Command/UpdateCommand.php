<?php

namespace Dockify\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Dockify\Creator;
use Dockify\Command\BaseCommand as Command;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateCommand extends Command
{
    const COMMAND_NAME = 'update';

    /**
     * @var SymfonyStyle
     */
    protected $io;

    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
             ->setDescription('Update dockify or selected creator');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);

        $target = $this->io->choice('Select service to be updated', array_merge(
            ['all', 'self'],
            array_keys(Creator::$knownCreators)
        ));


        if ($target === 'all') {
            $this->io->title('Updating dockify');
            $this->updateSelf();
            $this->io->success('Dockify successfully updated');

            foreach (Creator::$knownCreators as $creator => $registry) {
                $this->io->title('Updating ' . $creator);
                $this->updateCreator($creator);
                $this->io->success(ucfirst($creator) . ' successfully updated');
            }
        } elseif ($target === 'self') {
            $this->updateSelf();
        } else {
            $this->updateCreator($target);
        }
    }

    /**
     * @throws \Exception
     */
    private function updateSelf()
    {
        $containerID = file_get_contents('/etc/hostname');

        if ($containerID === false) {
            throw new \Exception('Error reading container hostname');
        }

        $image = $this->docker->getContainerImage($containerID);

        $this->io->note("Updating $image");
        
        $this->docker->pull($image);
    }

    private function updateCreator(string $creatorName)
    {
        $creator = new Creator($creatorName);

        $this->docker->pull($creator->getDockerImage());
    }
}
