<?php

namespace Dockify\Command;

use Dockify\Config;
use Dockify\Docker\Facade\DockerFacade;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BaseCommand extends Command
{
    /**
     * @var DockerFacade
     */
    protected $docker;

    /**
     * @var Config
     */
    protected $config;

    public function __construct(DockerFacade $docker, Config $config, string $name = null)
    {
        parent::__construct($name);

        $this->docker = $docker;
        $this->config = $config;
    }

    protected function exitWithError(OutputInterface $output, string $message, int $code = 1)
    {
        $output->writeln("<error>$message</error>");
        exit($code);
    }
}
