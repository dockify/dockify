<?php

declare(strict_types=1);

namespace Dockify\Command;

use Dockify\Creator;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Dockify\Command\BaseCommand as Command;
use Symfony\Component\Process\Exception\ProcessFailedException;

class RunCommand extends Command
{
    const COMMAND_NAME = 'run';

    /**
     * @var SymfonyStyle
     */
    protected $io;

    protected function configure()
    {
        $this->setName(self::COMMAND_NAME);

        /**
         * Default public working directory the creator
         * will use. This is not meant to be overridden by
         * user's input. This used to pass system current $PWD
         * Users should use `pwd` option.
         */
        $this->addOption(
            'default-pwd',
            null,
            InputOption::VALUE_REQUIRED,
            'Default pwd option'
        );

        /**
         * Default user id, that will be used by the creator.
         * Needs to be set so newly created files can be set with
         * proper ownership rights. Shouldn't be used by users, see `default-pwd`.
         */
        $this->addOption(
            'default-uid',
            null,
            InputOption::VALUE_REQUIRED,
            'Default uid option'
        );

        /**
         * Default public working directory provided by user.
         */
        $this->addOption(
            'pwd',
            null,
            InputOption::VALUE_REQUIRED,
            'Working directory dockify will use.'
        );

        /**
         * Default user id provided by user.
         */
        $this->addOption(
            'uid',
            null,
            InputOption::VALUE_REQUIRED,
            'User ID the creator container will run under.'
        );

        $this->setDescription('Run dockify');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);

        $pwd = $input->getOption('pwd') ?? $input->getOption('default-pwd');
        $uid = $input->getOption('uid') ?? $input->getOption('default-uid');

        if (!is_string($pwd)) {
            throw new \Exception('Working directory must be string');
        }

        if (!is_string($uid)) {
            throw new \Exception('User ID must be string');
        }

        $this->greetUser();

        $options = array_merge(
            array_keys(Creator::$knownCreators),
            ['custom', 'exit']
        );

        $choice = $this->io->choice('Select creator to use', $options);

        switch ($choice) {
            case 'exit':
                $this->io->text('Bye!');
                exit;
            case 'custom':
                $creatorName = $this->io->ask('Enter path to your creator\'s registry');
                break;
            default:
                $creatorName = $choice;
        }

        if (empty($creatorName)) {
            $this->io->text('Bye!');
            exit;
        }

        $creator = new Creator($creatorName);

        try {
            $volumeName = sprintf(
                '%s-%s',
                $this->config->get('volume-name', 'dockify-storage'),
                md5($creatorName)
            );

            // This needs to be done, because creator will not run
            // as a root user (it will run under users UID), so it
            // would not have permissions to write to the /cache folder.
            if (!$this->docker->volumeExists($volumeName)) {
                $this->docker->createVolume($volumeName);

                $this->docker->changeVolumeDirectoryOwnership(
                    $volumeName,
                    $creator->getCachePath(),
                    $uid
                );
            }

            $this->docker->runDockify($uid, $pwd, $creator, $volumeName);
        } catch (ProcessFailedException $exception) {
            $this->exitWithError($output, 'Process execution failed.');
        } catch (\Exception $exception) {
            $this->exitWithError($output, $exception->getMessage());
        }
    }

    protected function greetUser(): void
    {
        $greetings = [
            'Ahoy!',
            'Hello!',
            'Hi there!',
            'Howdy, partner!',
        ];

        $selectedGreeting = $greetings[array_rand($greetings)];

        $this->io->title($selectedGreeting);
    }
}
