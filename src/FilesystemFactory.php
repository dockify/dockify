<?php

namespace Dockify;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

class FilesystemFactory
{
    private $filesystem = null;

    public function create(): Filesystem
    {
        if ($this->filesystem === null) {
            $this->filesystem = new Filesystem(
                new Local(__DIR__ . '/../storage')
            );
        }

        return $this->filesystem;
    }
}
