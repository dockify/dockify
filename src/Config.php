<?php

namespace Dockify;

use Symfony\Component\Yaml\Yaml;

class Config
{
    /**
     * @var bool
     */
    private $init = false;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var string
     */
    private $configFilePath = null;

    public function __construct(string $filePath)
    {
        $this->configFilePath = $filePath;
    }

    public function get(string $key, $default = '')
    {
        $this->initIfEmpty();

        return array_key_exists($key, $this->data)
            ? $this->data[$key]
            : $default;
    }

    private function initIfEmpty()
    {
        if ($this->init === true) {
            return;
        }

        $this->data = Yaml::parseFile($this->configFilePath);
        $this->init = true;
    }
}
