<?php

namespace Dockify;

class Creator
{
    /**
     * @var string
     */
    protected $dockerImage;

    /**
     * @var string
     */
    protected $mountPoint = '/app';

    /**
     * @var string
     */
    protected $command = 'run';

    /**
     * @var string
     */
    protected $cachePath = '/cache';

    /**
     * @var array
     */
    public static $knownCreators = [
        'laravel' => 'registry.gitlab.com/dockify/creators/laravel-creator',
        'semicreator' => 'registry.gitlab.com/dockify/creators/semicreator',
    ];

    public function __construct(string $creator)
    {
        $this->dockerImage = self::$knownCreators[$creator] ?? $creator;
    }

    /**
     * @return string
     */
    public function getDockerImage(): string
    {
        return $this->dockerImage;
    }

    /**
     * @return string
     */
    public function getMountPoint(): string
    {
        return $this->mountPoint;
    }

    /**
     * @return string
     */
    public function getCommand(): string
    {
        return $this->command;
    }

    /**
     * @return string
     */
    public function getCachePath(): string
    {
        return $this->cachePath;
    }
}
