#!/bin/bash

# This script is meant to be used for installation purposes
# see README.md

case "$1" in
    "run")
        cmd="run --default-uid=$UID --default-pwd=$PWD ${@:2}"
    ;;

    *)
        cmd="$@"
    ;;
esac

docker run \
    -it \
    -v /var/run/docker.sock:/var/run/docker.sock \
    --rm \
registry.gitlab.com/dockify/dockify ${cmd}
