# Dockify

## Content
- [Overview](#overview)
- [Showcase](#showcase)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Usage](#usage)
    - [Available built in creators](#available-built-in-creators)
    - [Using custom creators](#using-custom-creators)
    - [Example usage](#example-usage-of-laravel-creator)
- [Creating custom dockify creator](#creating-custom-dockify-creator)
- [Contribute](#contribute)

## Overview

Dockify allows you to **install** and **run** applications **without** any other
**dependencies** besides [docker](https://docker.com).

## Showcase

Don't take my word for it. Here's a complete installation of project on php's
framework [Laravel](https://laravel.com).

TODO: Add usage gif

## Prerequisites

1. Docker installed

        $ docker --version
        Docker version 18.09.5, build e8ff056

## Installation


1. Download the script file

        $ sudo curl -L "https://gitlab.com/dockify/dockify/raw/master/dockify_install.sh" -o /usr/local/bin/dockify

2. Give it executable permissions

        $ sudo chmod +x /usr/local/bin/dockify

3. Verify your installation

        $ dockify
        Console Tool

        Usage:
          command [options] [arguments]

        Options:
          -h, --help            Display this help message
          -q, --quiet           Do not output any message
          -V, --version         Display this application version
              --ansi            Force ANSI output
              --no-ansi         Disable ANSI output
          -n, --no-interaction  Do not ask any interactive question
          -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

        Available commands:
          help    Displays help for a command
          list    Lists commands
          run     Run dockify
          update  Update dockify or selected creator

## Usage

Dockify itself does not install requested applications.
It delegates it's creation to something called **dockify creator**.
Dockify creator is nothing but docker container designed to install app of your choice.
When using dockify, you can choose between **creators** that ship with dockify, 3rd party solution or creators of your own.

    $ dockify run
    Hi there!
    =========

     Select creator to use:
      [0] laravel
      [1] semicreator
      [2] custom
      [3] exit
     > 

### Available built in creators
- [Laravel creator](https://gitlab.com/dockify/creators/laravel-creator)

### Using custom creators

After running creator, select option 2 (custom) and enter path to your creator registry.

    $ dockify run
    Hi there!
    =========

     Select creator to use::
      [0] laravel
      [1] semicreator
      [2] custom
      [3] exit
     > 2

     Enter path to your creator's registry:
     > registry.gitlab.com/my-namespace/my-dockify-creator:latest

And that's it. This will setup fresh laravel installation for you.
Even though the example uses laravel, dockify is not limited to any specific tools or languages. You can build **dockify creators** for scaffolding PHP, Node, JavaScript, Python or any app you can think of.

## Creating custom dockify creator

Can not find creator of your taste?
You can create your own creator with dockify semicreator.

Run dockify and select `semicreator`.

    Ahoy!
    =====

     Select creator to use:
      [0] laravel
      [1] semicreator
      [2] custom
      [3] exit
     > 1

## Contribute
TODO
